<?php
	//Inicio clase documento
include("../Model/bookModel.php");
	
class Notebook{
	
	//Define variables de la clase
	
	private $notebook_id;
    private $notebook_user_id;
    private $notebook_name;
    private $notebook_description;
    private $notebook_date_register;
	
	//Constructor clase documento
	function Notebooks($id, $user_id, $name, $description, $date_register) {        
		$this->notebook_id				=	$id;
		$this->notebook_user_id			=	$user_id;
		$this->notebook_name			=	$name;
		$this->notebook_description		=	$description;
		$this->notebook_date_register	=	$date_register;
    }
	
	function insertNotebook(){	
		$objNote=new BookModel();			
		$result = $objNote->insert($this->notebook_id,$this->notebook_user_id,$this->notebook_name,$this->notebook_description,$this->notebook_date_register);	
		return $result;
		//return $result;
	}//Fin function insertNotebook
	
	function consultNotebook(){	
		$objNote=new BookModel();			
		$result = $objNote->consult($this->notebook_user_id); 
		
		return $result;
		//return $result;
	}//Fin function consultNotebook
		
	
}//Fin clase documento

	
	

?>
<?php 
	include("../Controller/bookController.php");
	@session_start() 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css"  href="../../css/css_general.css" rel="stylesheet" >
<link type="text/css" rel="stylesheet" href="../../css/jquery-ui.css" />
<script type="text/javascript" src="../../js/framewoks/jquery-1.9.1.js"></script>
<script type="text/javascript" src="../../js/framewoks/jquery.validate.min.js"></script>
<script type="text/javascript" src="../../js/framewoks/ui.js"></script>
<script type="text/javascript" src="../../js/jsMaster.js"></script>




<title>GYDNote</title>
</head>

<body>
	<div id="head">    
        <div id="headTop">
        	<div id="User" title="Click para actualizar tu informaci&oacute;n">User: <?php echo $_SESSION["email"]; ?> </div>
        </div>	
        <div id="search">
        	<form>
                <label>Buscar nota</label>
                <input type="text" id="searchNote" name="searchNote" title="Introduzca una nota..." />
                <input type="submit" value="Buscar"/>
            </form>
        </div>
    </div>
    
    <div id="content">
    
    	<div id="paneLeft">
        
        <ul id="menu">
          <li><a href="javascript:;" id="newNotebook" name"newNotebook"><img src="../../images/notebook.png" width="15" height="15"/> Crear Cuaderno</a></li>
          <li><a href="javascript:;" id="newNote" name"newNote"><img src="../../images/note.png" width="15" height="15"/> Crear Nota</a></li>
        </ul>
        
        </div>
        <div id="paneMid">
        	Cuadernos
        </div>
        <div id="paneRight">Notas</div>
    </div>
    <div id="newBook" title="Crear nuevo cuaderno">    
    <form id="formNotebook" name="formNotebook" method="post" action="">
	    <fieldset>
        	<input type="hidden" value="1" name="action" id="action" />
            <label for="newNameNotebook">Nombre </label><br />
            <input type="text" name="newNameBook" id="newNameBook" class="required text ui-widget-content ui-corner-all" /><br /><br />
            <label for="newDescrition">Descripcion </label><br />
            <textarea id="newDescriptionBook" name="newDescriptionBook" class="required"></textarea>
		</fieldset>
	</form>
    
    </div>
    <div id="foot">
    	GYDNote Derechos reservados @loz Sorroz 2013
    </div>
</body>
</html>

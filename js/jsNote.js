// JavaScript Document
jQuery(document).ready(function(e) {
    jQuery("#newNote").validate({
			//messages: {
		rules:{
			note_name:{
				minlength: 3
			},
			note_description:{
				minlength: 3
			},
			note_content:{
				minlength: 3
			}
			,
			note_tag:{
				minlength: 3
			}
		},
		messages: {
			note_name:{
				required : ' * Campo requerido',
				minlength : 'Minimo 3 caracteres'
			},
			note_description:{
				required : ' * Campo requerido',
				minlength : 'Minimo 3 caracteres'
			},
			note_content:{
				required : ' * Campo requerido',
				minlength : 'Minimo 3 caracteres'
			},
			note_tag:{
				required : ' * Campo requerido',
				minlength : 'Minimo 3 caracteres'
			},
			
		}
	});
	
	jQuery("#newNote").submit(function() {

		if (jQuery("#newNote").valid()) {				
			jQuery.ajax({
				type: 'POST',
				url:'../Business/noteBusiness.php',
				data: jQuery('#newNote').serialize(),
				success: function(dato){
					//alert(dato);
					document.getElementById("resultNote").innerHTML ='<i>'+dato+'</i>';	
				}
			});
				return false;
		} else {
		return false;
		}
			return false;
	
	});
});


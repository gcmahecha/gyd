jQuery(function() {
		
	jQuery( "#newBook" ).dialog({
		autoOpen: false,
		height	: 280,
		width	: 350,
		modal	: true,
		buttons	: {
		"Crear": function() {
			jQuery("#formNotebook").validate({
			rules:{
				newNameBook:{
					minlength: 3
				},					
				newDescriptionBook:{
					minlength: 3
				}
			},
			messages: {
				newNameBook:{
					required : ' * Campo requerido',
					minlength : 'Minimo 3 caracteres'
				},
				newDescriptionBook:{
					required : ' * Campo requerido',
					minlength : 'Minimo 3 caracteres'
				}
			}
		});
				
		//jQuery("#formNotebook").submit(function() {
	
			if (jQuery("#formNotebook").valid()) {				
					jQuery.ajax({
					type: 'POST',
					url:'../Controller/bookController.php',
					data: jQuery('#formNotebook').serialize(),
					success: function(dato){
						alert(dato);
						$("#newBook").dialog( "close" );
						
						
						
						
											
						//document.getElementById("resultNote").innerHTML ='<i>'+dato+'</i>';	
					}
				});
					return false;
			} else {
			return false;
			}
				return false;
		
		//});
		
		},
		Cancel	: function() {
		jQuery( this ).dialog( "close" );
		}
		},
		close	: function() {
			//allFields.val( "" ).removeClass( "ui-state-error" );
	}
	});

	jQuery( "#newNotebook" )
	.button()
	.click(function() {
		$( "#newBook" ).dialog( "open" );
	});
	
	jQuery( "#User" ).tooltip({
		track: true,
		show: {
			effect: "slideDown",
			delay: 250
		}
	});
	
	jQuery( "#searchNote" ).tooltip({
		track: true,
		show: {
			effect: "slideDown",
			delay: 250
		}
	});
	
	jQuery( "#menu" ).menu();
	
	
	
	
	
});

jQuery(document).ready(function(e) {
    jQuery("#login").validate({
			//messages: {
		rules:{
			password:{
				minlength: 6
			}
		},
		messages: {
			mail:{
				required : 'Digite una direcci&oacute;n de correo v&aacute;lida',
				email : 'La dirección de correo no tiene un formato v&aacute;lido'
			},
			password:{
				required : 'Digite su password',
				minlength: "El password debe contener por lo menos 6 caracteres"
			},
		}
	});
	
	jQuery("#login").submit(function() {
	
		if (jQuery("#login").valid()) {
			jQuery.ajax({
				type: 'POST',
				url:'php/Controller/userController.php',
				data: jQuery('#login').serialize(),
				success: function(dato){
					if( dato == 0 )
						document.getElementById("result").innerHTML ='<i> Usuario o contraseña inválido. </i>';	
					else if ( dato == 1 )
						window.location.href = 'php/View/viewMaster.php';
					else
						document.getElementById("result").innerHTML ='<i> Ha ocurrido un error</i>';	
				}
			});
				return false;
		} else {
		return false;
		}
			return false;
	
	});
});